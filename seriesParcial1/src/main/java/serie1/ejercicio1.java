package serie1;
import rx.Observable;
import rx.Observer;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.observables.MathObservable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;



public class ejercicio1 {

    public static void main(String[] args) {
        List<Productos> productos = new ArrayList<>();
        List<Productos> productosA = new ArrayList<>();
        String pA;

        productos.add(new Productos("ps4", 300));
        productos.add(new Productos("gamecube", 300));
        productos.add(new Productos("external disk", 200));
        productos.add(new Productos("laptop", 800));
        productos.add(new Productos("vr", 230));

        Observable sumatorioProductos = Observable.from(productos.toArray())
                .map((result) -> {
                    Productos producto = (Productos) result;
                    return producto.getPrecio();
                }).reduce(
                        new Func2<Integer, Integer, Integer>() {
                            @Override
                            public Integer call(Integer acumulador, Integer actual) {
                                return acumulador + actual;
                            }
                        }
                );
        sumatorioProductos.subscribe((sumatoria) -> {
            System.out.println("La sumatoria de los productos es: " + sumatoria);
        });

        Observable<Productos> maximoProducto = Observable.from(productos);

        MathObservable.from(maximoProducto).max(Comparator.comparing(Productos::getPrecio))
                .subscribe(new Observer<Productos>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Productos productos) {
                        System.out.println("El producto con maximo precio es: " + productos.getNombre() + ", " + productos.getPrecio());
                    }
                });



        Observable sumatoriaA = Observable.from(productos.toArray());

        Observable resltA = sumatoriaA
                .map((result) -> {
                    Productos producto = (Productos) result;

                    return producto.getNombre();
                }).filter(
                        new Func1<String, Boolean>() {

                            @Override
                            public Boolean call(String s) {
                               return s.contains("a");

                            }
                        }
                );





            resltA.subscribe((sumaA)->{
                Integer precioFinal = 0;
                    for (Productos u: productos){
                        if (sumaA.equals(u.getNombre())){
                            productosA.add(u);
                        }
                    }
                for (Productos uF:productosA){
                    precioFinal += uF.getPrecio();
                }
                System.out.println("La sumatoria del los productos con A es: "+precioFinal);
            });





    }

}
