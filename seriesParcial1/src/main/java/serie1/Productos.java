package serie1;

public class Productos {
    private String nombre;
    private int precio;
    private int id;

    public Productos(String nombre, int precio){
        this.nombre = nombre;
        this.precio = precio;
    }

    public int getPrecio(){
        return precio;
    }

    public void setPrecio(int precio){
        this.precio = precio;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
