package Serie2;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.observables.MathObservable;

public class ejercicio2 {
    public static void main(String[] args){
        Integer[] numbers ={2, 5, 6, 8, 10, 35,  2, 10};
        Integer resultado;

        Observable<Integer>observablePromedio = Observable.from(numbers);



        Observable observableSumatoria = Observable.from(numbers)
                .reduce(
                        // siempre debo tener un acumulador y un valor actual
                        new Func2<Integer, Integer, Integer>() {
                            @Override
                            public Integer call(Integer anterior, Integer actual) {
                                Integer resultadoAcumulacion = anterior + actual;
                                return resultadoAcumulacion;
                            }
                        }
                );
        observableSumatoria.subscribe((sumatoria) -> {
            System.out.println("El resultado de la sumatoria es :" + sumatoria);

        });

        Observable<Integer> ObservablePromedio = Observable.from(numbers);

        MathObservable.averageInteger(ObservablePromedio)
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Integer integer) {
                        System.out.println("El promedio es: " + integer);
                    }
                });



        Observable<Integer> observableMayorOigual = Observable.from(numbers);

        Observable resultadoMap =
                observableMayorOigual
                        .map((item) -> {
                            if (item>=10){

                                return item;
                            }
                                return "Es menor";
                        });

        resultadoMap.subscribe((item) -> {
            System.out.println("Numero " + item);
        });




    }

}

